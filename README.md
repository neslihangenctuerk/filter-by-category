This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


## General & Implementation details
The goal is to have a reusable ShoppingCart and a reusable Filter Component.
Currently you can select a category from the dropdown (chairs, sofas, all) and view only items that match that tag. 
Ideally there should be multiple filters that can be applied at the same time, like __color__ and __category__ for example.

So far only __ProductSection__ holds state and function implementations. Although not implemented as functional components the other components are mainly concerned with rendering props that were passed to them and signaling button clicks to the parent component.




__next possible TODOS__: 
+ add multiple filter functionality __[DONE]__ _(11.2.2020)_

+ add styling primarily to the __ProductCard__ and __ShoppingCart__

_new (11.2.2020):_
+ refactor __FilterByCategory__ and __FilterByColor__ into 1 component ? __(priority)__
+ add delete option to cart items __(priority)__

+ add count on cart items
+ handle case: "if product already in cart" - update UI via message or disable button or increment cart item
+ add increment and decrement on cart items

+ add price to products
+ show sum of current shoppingcart





## Installation and Usage

### `npm install`


### `npm start`
This runs the app in development mode.
Visit [http://localhost:3000](http://localhost:3000) in the browser.



## Tools
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). Which is licensed under: [LICENSE](https://github.com/facebook/create-react-app/blob/master/LICENSE). 
Also there is a copy of this license under __NOTICE.md__ in this project folder.


## License
[MIT](https://gitlab.com/neslihangenctuerk/filter-by-category/-/blob/master/LICENSE)

