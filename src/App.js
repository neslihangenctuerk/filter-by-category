import React from 'react'
import './App.css'
import ProductSection from './ProductSection'


function App() {
  return (
    <div className="App">

      <ProductSection />
      
    </div>
  );
}

export default App;
