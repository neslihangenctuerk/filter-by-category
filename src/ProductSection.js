import React from 'react'
import ProductCard from './ProductCard'
import { 
         CATEGORY_SOFAS,
         COLOR_PINK, 
         CATEGORY_CHAIRS, 
         COLOR_BLUE, 
         CATEGORY_TABLES, 
         COLOR_DARK_BROWN, 
         CATEGORY_ALL,
         COLOR_ALL
       } from './constants'
import ShoppingCart from './ShoppingCart'
import FilterByCategory from './FilterByCategory'
import FilterByColor from './FilterByColor'



class ProductSection extends React.Component {

    state = {
        products: [
            {
                id: 0,
                title: 'Velvet Couch',
                category: CATEGORY_SOFAS,
                color: COLOR_PINK
            },
            {
                id: 1,
                title: 'Minimalista Chair',
                category: CATEGORY_CHAIRS,
                color: COLOR_BLUE
            },
            {
                id: 2,
                title: 'Balconia Chair',
                category: CATEGORY_CHAIRS,
                color: COLOR_PINK
            },
            {
                id: 3,
                title: 'Mahagony Sidetable',
                category: CATEGORY_TABLES,
                color: COLOR_DARK_BROWN
            }
        ],
        shoppingCart: [],
        selectedCategory: CATEGORY_ALL,
        selectedColor: COLOR_ALL
    }

    handleOnClick = (selectedProduct) => {
        this.setState((state) => {
            return {
                shoppingCart: [...state.shoppingCart, selectedProduct]
            }
        })
    }

    handleFilterChange = (selection) => {
        const target = selection.target.name
        const value = selection.target.value
        console.log("Target property to update: ", target)
        console.log("Value of target to update: ", value)
        this.setState((state) => {
            return {
                [target]: value
            }
        })
    }


    render() {
        let filteredList

        if (this.state.selectedCategory !== CATEGORY_ALL) {
            // get selected category only in the selected color:
            if (this.state.selectedColor !== COLOR_ALL) {
                console.log("get selected category only in the selected color")
                filteredList = this.state.products.filter(product => product.category === this.state.selectedCategory)
                                        .filter(product => product.color === this.state.selectedColor)
                                            .map(product =>
                                                <ProductCard
                                                    key={product.id}
                                                    onClick={this.handleOnClick}
                                                    title={product.title}
                                                    category={product.category}
                                                    color={product.color}
                                                />
                                            )

            // get selected category in all colors:
            } else {
                filteredList = this.state.products.filter(product => product.category === this.state.selectedCategory)
                                        .map(product =>
                                            <ProductCard
                                                key={product.id}
                                                onClick={this.handleOnClick}
                                                title={product.title}
                                                category={product.category}
                                                color={product.color}
                                            />
                                        )
            }
            
        } else if(this.state.selectedCategory === CATEGORY_ALL) {
            // get all categories in selected color
            if(this.state.selectedColor !== COLOR_ALL){
                filteredList = this.state.products.filter(product => product.color === this.state.selectedColor)
                                                        .map(product => 
                                                            <ProductCard
                                                                key={product.id}
                                                                onClick={this.handleOnClick}
                                                                title={product.title}
                                                                category={product.category}
                                                                color={product.color}
                                                            />
                                                        )

            // get all categories in all colors                                           
            } else {  
                filteredList = this.state.products.map(product => 
                                                            <ProductCard
                                                                key={product.id}
                                                                onClick={this.handleOnClick}
                                                                title={product.title}
                                                                category={product.category}
                                                                color={product.color}
                                                            />
                                                      )
            }     
        }

        return (
            <div>
                <h3>ProductSection Component</h3>
                <FilterByCategory
                    selectedCategory={this.state.selectedCategory}
                    handleFilterChange={this.handleFilterChange}

                />

                <FilterByColor
                    selectedColor={this.state.selectedColor}
                    handleFilterChange={this.handleFilterChange}

                />

                {
                    this.state.selectedCategory &&
                    <div>
                        {this.state.selectedCategory}
                    </div>
                }

                { filteredList }


                {
                    this.state.shoppingCart &&
                        <ShoppingCart 
                            cart={this.state.shoppingCart}
                        />
                }
                
            </div>
        )
    }
}

export default ProductSection