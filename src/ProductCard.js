import React from 'react'

class ProductCard extends React.Component {

    onClicked = (event) => {
        this.props.onClick(event.target.value)
    }

    render() {
        return (
            <div>
                <h3>ProductCard Component</h3>

                <div>{this.props.title}</div>
                <div>{this.props.category}</div>
                <button value={this.props.title} onClick={this.onClicked}>Add to Cart</button>
            </div>
        )
    }
}
export default ProductCard