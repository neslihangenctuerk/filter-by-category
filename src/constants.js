export const CATEGORY_SOFAS = "0"
export const CATEGORY_TABLES = "1"
export const CATEGORY_CHAIRS = "2"
export const CATEGORY_BEDS = "3"
export const CATEGORY_ALL = "4"


// Refactor later to use Hex Code Values 
export const COLOR_PINK = "pink"
export const COLOR_YELLOW = "yellow"
export const COLOR_BLUE = "blue"
export const COLOR_DARK_BROWN = "darkbrown"
export const COLOR_ALL = "all"