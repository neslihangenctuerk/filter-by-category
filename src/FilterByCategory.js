import React from 'react'
import { 
    CATEGORY_ALL, 
    CATEGORY_CHAIRS, 
    CATEGORY_SOFAS, 
    CATEGORY_TABLES } from './constants'

class FilterByCategory extends React.Component {

    handleFilterChanged = (event) => {
        this.props.handleFilterChange(event)
    }


    render() {
        return (
            <div>
                <div>FilterByCategory Component</div>

                <select 
                    name="selectedCategory"
                    value={this.props.selectedCategory} 
                    onChange={this.handleFilterChanged} 
                >
                        <option value={CATEGORY_ALL}>All</option>
                        <option value={CATEGORY_CHAIRS}>Chairs</option>
                        <option value={CATEGORY_SOFAS}>Sofas</option>
                        <option value={CATEGORY_TABLES}>Tables</option>

                </select>
            </div>
        )
    }
}

export default FilterByCategory