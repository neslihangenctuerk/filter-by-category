import React from 'react'
import { COLOR_ALL, COLOR_PINK, COLOR_YELLOW, COLOR_BLUE, COLOR_DARK_BROWN } from './constants'

class FilterByColor extends React.Component {

    handleFilterChanged = (event) => {
        this.props.handleFilterChange(event)
    }


    render() {
        return (
            <div>
                <div>FilterByColor Component</div>

                <select 
                    name="selectedColor"
                    value={this.props.selectedColor} 
                    onChange={this.handleFilterChanged} 
                >
                        <option value={COLOR_ALL}>all</option>
                        <option value={COLOR_PINK}>pink</option>
                        <option value={COLOR_YELLOW}>yellow</option>
                        <option value={COLOR_BLUE}>blue</option>
                        <option value={COLOR_DARK_BROWN}>darkbrown</option>

                </select>
            </div>
        )
    }
}

export default FilterByColor